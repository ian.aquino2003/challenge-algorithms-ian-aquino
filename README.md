## Desafio 2 M3 Academy

Desafio de iniciação ao Javascript.

- [x] greeting
- [x] area-do-triangulo
- [x] max-value
- [x] fibonnaci
- [x] é-primo
- [x] soma-dos-elementos
- [x] soma-dos-pares
- [x] caractere-mais-repetido
- [x] anagrama
